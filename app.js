const Web3 = require("web3");
const namehash = require('eth-ens-namehash');
const RNS_ABI = require('./RNS.json')

const RNSAddress = "0x0fec107937Be14DF4436448Eba5179b1f2dF0d43";

const gasPrice = '123456';

const TLD = {
    plain: 'didi.rsk',
    hash: namehash.hash('didi.rsk') 
};

class RNSManager {
    constructor( url, port, debug = false){

        this.server = `http://${url}:${port}`;
        this.web3Provider = null;
        this.web3 = null;

        this.debug = debug;

        this.RNS = null;

        this.init();

    }

    log = async (message) => {
        if( this.debug ) console.log(`*** ${message} ***`)
    }

    init = async () => {
        // Initialize libraries and RNSContract

        await this.initWeb3();
        await this.initRNSContract()
    }

    initWeb3 = async () =>   {
        //Initialize web3 library
        this.web3 = new Web3(new Web3.providers.HttpProvider(this.server));
    }

    initRNSContract = async () => {
        //Initialize RNS contract with ABI and address

        const { web3, log } = this;

        this.RNS = await new web3.eth.Contract(RNS_ABI.abi, RNSAddress, { gasPrice })

        log("Contract initialized")

    }

    getOwner = async (node) => {


        const { RNS, log } = this;
        const { plain: plainTLD } = TLD;

        let domain = `${node}.${plainTLD}`;

        log(`Getting owner of ${domain}`);

        domain = namehash.hash(domain)

        const owner = await RNS.methods.owner(domain).call()

        if ( !owner ) throw new Error( "Unexpected error: somethig weird happened here :(")

        log(`The owner is <${owner}> ( returning owner )`)

        return owner;

    }

    createSubnode = async ( plainSubDomain, newOwnerAddress, force = false )  => {
        // Force argument forces to change the owner of that node if it is already owned by someone else

        const { RNS, log, web3, getOwner } = this;
        const { hash: hashedTLD, plain: plainTLD } = TLD;

        log(`Creating a subnode for ${plainTLD} under the name of ${plainSubDomain} pointing to <${newOwnerAddress}>`)

        const currentOwner = await getOwner(plainSubDomain);

        if( !force && currentOwner !== "0x0000000000000000000000000000000000000000" ) throw new Error(`There is already an owner for that subdomain`)

        const subDomain = web3.utils.sha3(plainSubDomain);

        const response = await RNS.methods.setSubnodeOwner(hashedTLD, subDomain, newOwnerAddress).send({from: '0xf34ECF77F483261441Ec373dA9446E0011457c68'})

        if ( !response ) throw new Error( "Unexpected error: transaction failed" )

        const domain = `${plainSubDomain}.${plainTLD}`

        log(`Created subnode ${domain} and the owner was changed from <${currentOwner}> to <${newOwnerAddress}>  ${force ? '--this was a forced owner update' : '' } ( returning domain )`)

        return domain;
            
    }

    createWallet = async () => {
         
        
        const { web3, log } = this;
        
        log(`Creating a new Wallet!`)

        const response  = await web3.eth.accounts.create();

        if( !response ) throw new Error("Error when creating your wallet, maybe you should try again later :,( ")

        const { address } = response;

        log(`Your new Wallet address is <${address}> ( returning addres )`)
        
        return address;

    }
}




const usage = async (host, port, debug = false) => {
    RNS = await new RNSManager( host, port, debug );

    try {
        const wallet = await RNS.createWallet()

        const newSubnode = await RNS.createSubnode("pepe", wallet, true)
    }catch ( e ) {
        console.log(e.message)
    }

}


usage('localhost', '7545', true)